﻿using System;
using System.Text;
using System.IO;

namespace TwitchChatBotTry
{
    class WordFilter
    {
        private string[] Words = new string[200];
        private byte NumOfWords = 0;

        public void Inicialization()
        {
            string FilePath = "Files/BannedWords.txt";
            if (!File.Exists(FilePath))
            {
                Console.WriteLine("Отсутствует файл с командами чата. Создан новый. {0}", FilePath);
                Console.WriteLine("\r\nВписывать забаненые словo/словосочитание в строку");
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(FilePath));
                    FileStream NewFile = File.Create(@FilePath);
                    NewFile.Close();
                }
            }
            FileStream file = new FileStream(@FilePath, FileMode.Open);
            StreamReader reader = new StreamReader(file, Encoding.Unicode);
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                Words[NumOfWords] = line.Trim();
                NumOfWords++;
            }
            reader.Close();
            file.Close();
        }

        public bool HasBannedWords(string ChatMessage)
        {
            for (byte i = 0; i < NumOfWords; i++)
            {
                if (ChatMessage.Contains(Words[i])) return true;
            }
            return false;
        }

        public string BanMessage(string Name, byte Time)
        {
            Name = Name.Substring(0,Name.Length - 1);
            string line = "/timeout " + Name + " " + Time;
            return line;
        }
    }
}
