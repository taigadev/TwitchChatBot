﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace TwitchChatBotTry
{
    class SynchDataMmr
    {
        //private string line;
        private string[] items = new string[4];
        private string[] TryRequest(string[] mass)           // выдает нам 200 мест с топа таблицы в массив
        {
            string Site = "http://www.dota2.com/webapi/ILeaderboard/GetDivisionLeaderboard/v0001?division=europe";
            WebClient client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

            Stream data = client.OpenRead(Site);
            StreamReader reader = new StreamReader(data);

            string Text = reader.ReadLine();

            Int32 IndF = (Int32)Text.IndexOf("\"rank\":");
            Int32 IndL = (Int32)(Text.IndexOf("\"rank\":", IndF + 1) - 3);
            mass[0] = Text.Substring(IndF, IndL - IndF);

            for (byte i = 1; i < 199; i++)
            {
                IndF = (Int32)Text.IndexOf("\"rank\":", IndL + 1);
                IndL = (Int32)(Text.IndexOf("\"rank\":", IndF + 1) - 3);
                mass[i] = Text.Substring(IndF, IndL - IndF);
            }
            IndF = (Int32)Text.IndexOf("\"rank\":", IndL + 1);
            mass[199] = Text.Substring(IndF);
            mass[199] = mass[199].Substring(0, mass[199].IndexOf("solo_mmr") + 14);

            reader.Close();
            data.Close();

            return mass;
        }

        private void getAllFromMass(string line)         // выдает цифры из строки в массив из 4 элементов
        {
            string[] All = new string[4];

            // rank
            Int16 NumOfSymF = (Int16)(line.IndexOf("rank", 0) + 6);
            Int16 NumOfSymL = (Int16)line.IndexOf(",", NumOfSymF);
            items[0] = line.Substring(NumOfSymF, NumOfSymL - NumOfSymF);

            // name
            NumOfSymF = (Int16)(line.IndexOf("name", NumOfSymL) + 7);
            NumOfSymL = (Int16)(line.IndexOf(",", NumOfSymF) - 1);
            items[1] = line.Substring(NumOfSymF, NumOfSymL - NumOfSymF);

            // team_tag
            try
            {
                NumOfSymF = (Int16)(line.IndexOf("team_tag", NumOfSymL) + 11);
                NumOfSymL = (Int16)(line.IndexOf(",", NumOfSymF) - 1);
                items[2] = line.Substring(NumOfSymF, NumOfSymL - NumOfSymF);
            }
            catch
            {
                items[2] = null;
            }

            // solo_mmr
            NumOfSymF = (Int16)(line.IndexOf("solo_mmr", NumOfSymL) + 10);
            items[3] = line.Substring(NumOfSymF);
        }

        public void getNumbers(string Name)               // возвращает строку с уже вставленными элементами        
        {
            string[] mass = new string[200];
            TryRequest(mass);

            for (byte i = 1; i < mass.Length; i++)  // ищет с первого, потому что нулевая строка ничего не содержит
            {
                if (mass[i].ToLower().Contains(Name.ToLower()))
                {
                    getAllFromMass(mass[i]);           // rank; name; team; mmr
                }
            }
        }

        public string[] RequestRank()
        {
            return items;
        }
    }
}
