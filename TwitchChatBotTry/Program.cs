﻿using System;
using System.Threading.Tasks;
using System.IO;

namespace TwitchChatBotTry
{
    class Program
    {
        static void Main(string[] args)
        {
            IrcClient irc = new IrcClient();
            CommandTextReader txt = new CommandTextReader();
            SynchDataMmr mmr = new SynchDataMmr();
            AsynchTimer timer = new AsynchTimer();
            WordFilter filter = new WordFilter();


            irc.ReconnectToServer(txt.getChannelName(), txt.getBotName(), txt.getAuthCode());
            irc.JoinRoom();
            txt.InicializationFile();
            mmr.getNumbers(txt.getPlayerName());
            timer.Inicialization(txt.NumOfBasicComs);
            filter.Inicialization();
            
            if (!txt.UsedCreative)
                Console.WriteLine("Бот успешно запущен!" + Environment.NewLine
                + "Ниже ты будешь видеть команды, которые отдает бот\n\r");
            
            try
            {
                while(true)
                {
                    var message = irc.readMessage();

                    if (irc.getText(message).StartsWith("!"))
                    {
                        if (txt.CheckCommand(irc.getText(message)) != 255)
                        {
                            if (timer.IsCooldown(txt.CheckCommand(irc.getText(message))))     // false = cooldown
                            {
                                // rank; name; team; mmr
                                string[] items = mmr.RequestRank();
                                irc.SendMessage(txt.RequestMessage(irc.getName(message), irc.getText(message), items[0], items[1], items[2], items[3]));
                                timer.SetTimer(30, txt.CheckCommand(irc.getText(message)));
                                //                   Console.WriteLine("Сообщение: {0}", txt.RequestMessage(irc.getName(message), irc.getText(message)));
                            }
                            else Console.WriteLine("Перезарядка у команды");
                        }
                        else Console.WriteLine("Команда не найдена");
                    }
                    else
                    {           // фильтры на слова и тд
                        if (filter.HasBannedWords(irc.getText(message)))
                        {
                            //irc.SendMessage(filter.BanMessage(irc.getName(message), 60));
                            Console.WriteLine(filter.BanMessage(irc.getName(message), 60));
                        }
                    }
                }
            }
            catch 
            { 
                Console.WriteLine("\r\nЗаполните все поля в настройках бота, затем его открывайте");
                Console.WriteLine("ВО ВСЕХ ФАЙЛАХ СОХРАНЯЙТЕ С КОДИРОВКОЙ UTF-8");
                Console.WriteLine("Нажмите любую клавишу для выхода...");
                Console.ReadKey();
            }
        }
    }
}
