﻿using System;
using System.Threading.Tasks;
using System.Timers;

namespace TwitchChatBotTry
{
    class AsynchTimer
    {
        private bool[] EnabledCommands = new bool[200];
        private byte CurrectCommand;

        public void Inicialization(byte NumOfCommands)
        {
            for (byte i = 0; i < NumOfCommands; i++) EnabledCommands[i] = false;
        }

        public void SetTimer(int time, byte Num)
        {
            int Newtime = time * 1000;
            CurrectCommand = Num;
            EnabledCommands[CurrectCommand] = true;
            Timer timer = new Timer(Newtime);
            timer.Elapsed += EventTimer;
            timer.Enabled = true;
            timer.AutoReset = false;
        }

        private void EventTimer(Object sourse, ElapsedEventArgs e)
        {
            EnabledCommands[CurrectCommand] = false;
        }

        public bool IsCooldown(byte Num)
        {
            if (EnabledCommands[Num] == false) return true; 
                else return false;

        }
    }
}
