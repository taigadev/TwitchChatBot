﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TwitchChatBotTry
{
    class IrcClient
    {
        private TcpClient tcpClient;
        private StreamReader imputStream;
        private StreamWriter outputStream;
        private string userName, password, ChannelName, chatMessagePrefix;

        public void ReconnectToServer(string channelname, string botName, string oauth)         // авторизация
        {
            tcpClient = new TcpClient("irc.chat.twitch.tv", 6667);
            imputStream = new StreamReader(tcpClient.GetStream());
            outputStream = new StreamWriter(tcpClient.GetStream());

            this.userName = botName.ToLower();
            this.password = oauth;
            ChannelName = channelname.ToLower();
            
            chatMessagePrefix = ":" + userName + "!" + userName + "@"
                + userName + ".tmi.chat.twitch.tv PRIVMSG #" + ChannelName + " :";

            outputStream.WriteLine("PASS " + password + Environment.NewLine
                + "NICK " + userName + Environment.NewLine
                + "USER " + userName + " 8 * :" + userName);
            outputStream.Flush();
            
        }

        public void JoinRoom()                  // заход в комнатку чата
        {
            outputStream.WriteLine("JOIN #" + ChannelName);
            outputStream.Flush();
        }

        public string DeleteOtherSymbols(ref string message)
        {
            try
            {
                if (message.StartsWith(":"))
                {
                    byte NumOfSymInName = (byte)(message.IndexOf("!", 1) - 1);
                    if (NumOfSymInName < 1) NumOfSymInName = 0;
                    byte NumOfTrashSym = (byte)(message.IndexOf(":", 1));
                    if (NumOfTrashSym < 1) NumOfTrashSym = 0;
                    string messageTrash = message.Substring(1, NumOfSymInName)
                        + message.Substring(NumOfTrashSym);
                    message = messageTrash;
                }
            }
            catch { };
            return message;
        }

        private void SendItrMessage(string message)
        {
            outputStream.WriteLine(message);
            outputStream.Flush();
        }

        public void SendMessage(string message)
        {
            SendItrMessage(chatMessagePrefix + message);
            Console.WriteLine("{0}", message);
        }

        public string readMessage()
        {
            return imputStream.ReadLine();  
        }

        public string getName(string message)                       // ник пишущего с ! на конце ( marzipan! )
        {
            byte NumOfSymInName = (byte)(message.IndexOf("!"));
            return message.Substring(1, NumOfSymInName);
        }

        public string getText(string message)                       // текст пишущего 
        {
            try
            {
                byte PosOfSymInText = (byte)(message.IndexOf(":", 2) + 1);
                string ReTurn = message.Substring(PosOfSymInText);
                return ReTurn;
            }
            catch { return message; };
        }

    }
}
