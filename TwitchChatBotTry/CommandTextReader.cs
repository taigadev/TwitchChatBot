﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TwitchChatBotTry
{
    class CommandTextReader
    {
        private string FilePath;
        public byte NumOfBasicComs;
        private string[] OutCom = new string[224];
        private string[] InCom = new string[224];
        public bool UsedCreative = false;
        
        public void InicializationFile()
        {
            NumOfBasicComs = 0;                                                                                    
            FilePath = "Files/BasicCommands.txt";
            if (!File.Exists(FilePath))
            {
                Console.WriteLine("Отсутствует файл с командами чата. Создан новый. {0}", FilePath);
                Console.WriteLine("\r\nВписывать команды нужно следующим образом:" + Environment.NewLine
                    + "====================================" + Environment.NewLine + "!пнх" + Environment.NewLine
                    + "Пошел нахуй, -name-" + Environment.NewLine + "\r\n");
                Console.WriteLine("!стример" + Environment.NewLine
                    + "наш стример - лучший в мире!" + Environment.NewLine
                    + "====================================" + Environment.NewLine 
                    + "Примечание: \r\n1. Между командами разделяющий перенос строки"
                    + Environment.NewLine + "2. Команды, на которые отвлекается бот начинаются с !"
                    + Environment.NewLine + "3. Знаки === использованы для вас, а не для работы программы"
                    + Environment.NewLine + "4. Вместо -name- бот вставляет ник пишущего");
                CreateNewComFile(FilePath);
            }
            ReadAllCommands(FilePath, ref NumOfBasicComs);
        }

        private void ReadAllCommands(string FilePath, ref byte INT)      // INT - кол-во команд в файле      ХЗ
        {
            this.FilePath = FilePath;
            byte Count = 0;

            FileStream unputLines = new FileStream(@FilePath, FileMode.Open);
            StreamReader reader = new StreamReader(unputLines, Encoding.Unicode);
            string Line;
            byte k3 = 0;                                        // k3 - сччетчик

            while ((Line = reader.ReadLine()) != null)
            {
                if (k3 == 0) { InCom[Count] = Line; k3++; }              // 0 строка идет в команду "!команда"
                else if (k3 == 1) { OutCom[Count] = Line; k3++; }        // 1 строка идет в ответ на команду "ответ"
                else { Count++; k3 = 0; }        // 2 строка пропускается т.к. она пустая из-за форматирования текстовика
            }

            reader.Close();
            unputLines.Close();
            Count++;
            INT = Count;
        }

        private byte Info()
        {
            return NumOfBasicComs;
        }           // кол-во заюзанных команд
        
        private void CreateNewComFile(string FilePath)
        {
            this.FilePath = FilePath;
            Directory.CreateDirectory(Path.GetDirectoryName(FilePath));
            FileStream NewFile = File.Create(@FilePath);
            NewFile.Close();
            UsedCreative = true;
        }

        public byte CheckCommand(string Com)                   // тут тоже без лишнего текста, только команда !крм
        {
                for (byte k = 0; k < InCom.Length; k++)
                {
                    string comm = InCom[k];
                    if (comm == Com) return k;
                }
            return 255;
        }
                                                                 // rank; name; team; mmr
        public string RequestMessage(string Name, string Com, string rank, string player, string team, string mmr)       // тут тест без лишнего текста, только команда !крм
        {
            byte Num = CheckCommand(Com);
            string[] Words = new string[4];
            string[] Answers = new string[5];
              Words[0] = "-name-";
            Answers[0] = Name;
              Words[1] = "-player-";
            Answers[1] = player;
              Words[2] = "-rank-";
            Answers[2] = rank;
              Words[3] = "-mmr-";
            Answers[3] = mmr;
                                                                          // Name
            bool ContainsOrNot = false;
            string mess = null;
            for (byte i=0; i<4; i++)
            {
                string replaceWord = Words[i];
                if (OutCom[Num].Contains(replaceWord))
                {
                    if (mess == null) mess = OutCom[Num];
                    byte PosOfName = (byte)mess.IndexOf(replaceWord);
                    mess = mess.Substring(0, PosOfName) + Answers[i].Substring(0, Answers[i].Length) + mess.Substring(PosOfName + replaceWord.Length);
                    ContainsOrNot = true;
                }
            }
            if (ContainsOrNot) return mess;
            return OutCom[Num];
        }

        public string getChannelName()
        {
            this.FilePath = "Files/Settings/channelname.txt";
            if (!File.Exists(FilePath))
            {
                Console.WriteLine("Отсутствует файл выбора канала. Создан новый. {0}", FilePath);
                Console.WriteLine("Скопируйте название своего канала\r\n");
                CreateNewComFile(FilePath);
            }
            return File.ReadAllText(FilePath);
        }

        public string getBotName()
        {
            this.FilePath = "Files/Settings/botName.txt";
            if (!File.Exists(FilePath))
            {
                Console.WriteLine("Отсутствует файл с именем бота. Создан новый. {0}", FilePath);
                Console.WriteLine("Создайте нового пользователя twitch.tv с именем вашего бота.\r\n");
                CreateNewComFile(FilePath);
            }
            return File.ReadAllText(FilePath);
        }

        public string getAuthCode()
        {
            this.FilePath = "Files/Settings/botoauth.txt";
            if (!File.Exists(FilePath))
            {
                Console.WriteLine("Отсутствует файл с auth-кодом бота. Создан новый. {0}", FilePath);
                Console.WriteLine("Этот код нужно брать с сайта twitchapps.com/tmi\r\n");
                CreateNewComFile(FilePath);
            }
            return File.ReadAllText(FilePath);
        }

        public string getPlayerName()
        {
            this.FilePath = "Files/playername.txt";
            if (!File.Exists(FilePath))
            {
                Console.WriteLine("Отсутствует файл с именем игрока. Создан новый. {0}", FilePath);
                Console.WriteLine("Вписывать никнейм нужно с сайта dota2.com/leaderboards/#europe\r\n");
                CreateNewComFile(FilePath);
            }
            return File.ReadAllText(FilePath);
        }

    }
}
